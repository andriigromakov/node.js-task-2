require("dotenv").config();

const fs = require("fs");
const path = require("path");

const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const cors = require("cors");

const authRoutes = require("./routes/authRoutes");
const usersRoutes = require("./routes/usersRoutes");
const notesRoutes = require("./routes/notesRoutes");
// const authMiddleware = require("./midlewares/authMiddleware");

const PORT = +process.env.PORT;
const DB_CONNECTION_STRING = process.env.DB_CONNECTION_STRING;

const loggerFile = path.join(__dirname, "logs.log");

try {
  if (!fs.existsSync(loggerFile)) {
    fs.appendFileSync(loggerFile, "", "utf8");
  }
} catch (error) {
  console.log(error);
}

const app = express();

const accessLogStream = fs.createWriteStream(path.join(__dirname, "logs.log"), {
  flags: "a",
});

app.use(morgan("combined", { stream: accessLogStream }));

app.use(express.json());
app.use(cors());

app.use("/api/auth", authRoutes);
app.use("/api/users", usersRoutes);
app.use("/api/notes", notesRoutes);

app.use(function (err, req, res, next) {
  console.error(err.stack)
  if (err.status && err.message) {
    res.status(err.status).json({
      message: err.message,
    });
    return;
  }
  res.status( 500).json({
    message: "Internal server error",
  });
})

const initialize = async () => {
  try {
    await mongoose.connect(DB_CONNECTION_STRING, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    app.listen(PORT || 8080);
  } catch (err) {
    console.error(`Failed to start server: ${err.message}`);
    return;
  }
  console.log("server is running at port " + PORT);
};

initialize();
